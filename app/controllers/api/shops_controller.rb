class Api::ShopsController < ApplicationController
  def index
    @shops = Shop.where('name like ?', "%#{params[:name]}%").limit(3)
    if @shops.count > 0
      render 'index', formats: 'json', handlers: 'jbuilder'
    else
      render json: { error: { messages: ["Not found"] } }, status: :not_found
    end
  end
  def create
    @shop = Shop.new(shop_params)
    if @shop.save
      render nothing: true, status: :created
    else
      render json: { error: { messages: @shop.errors.full_messages } }, status: :bad_request
    end
  end

  def show
    if @shop = Shop.find_by(hwid: params[:id])
      render 'show', formats: 'json', handlers: 'jbuilder'
    else
      render json: { error: { messages: ["Not found"] } }, status: :not_found
    end
  end

  private

  def shop_params
    params.require(:shop).permit(:hwid, :name, :url, :description, :image_contents)
  end
end
