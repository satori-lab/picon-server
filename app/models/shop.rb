class Shop < ApplicationRecord
  has_one_attached :image
  validates :name, presence: true
  validates :hwid, uniqueness: true, presence: true
  validates :description, presence: true

  attr_accessor :image_contents
  after_create :parse_base64

  def parse_base64
    # If directly uploaded
    unless self.image_contents.nil? || self.image_contents[/(image\/[a-z]{3,4})|(application\/[a-z]{3,4})/] == ''
      content_type = self.image_contents[/(image\/[a-z]{3,4})|(application\/[a-z]{3,4})/]
      content_type = content_type[/\b(?!.*\/).*/]
      contents = self.image_contents.sub /data:((image|application)\/.{3,}),/, ''
      decoded_data = Base64.decode64(contents)
      filename = 'image_' + Time.zone.now.to_s + '.' + content_type
      File.open("#{Rails.root}/tmp/images/#{filename}", 'wb') do |f|
        f.write(decoded_data)
      end
      self.image.attach(io: File.open("#{Rails.root}/tmp/images/#{filename}"), filename: filename)
    end
  end
end
