json.hwids do
    json.array!(@shops) do |shop|
        json.hwid shop.hwid
    end
end