json.shop do
  json.hwid @shop.hwid
  json.name @shop.name
  json.description @shop.description
  json.url @shop.url if @shop.url
  json.image "https://picon-server.herokuapp.com" + url_for(@shop.image) if @shop.image.attached?
end
