Rails.application.routes.draw do
  namespace :api do
    resources :shops, only: [:create, :show, :index]
  end
end
