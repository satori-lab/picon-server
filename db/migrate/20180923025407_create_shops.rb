class CreateShops < ActiveRecord::Migration[5.2]
  def change
    create_table :shops do |t|
      t.string :hwid
      t.string :name
      t.string :url
      t.string :description, unique: true

      t.timestamps
    end
  end
end
