FactoryBot.define do
  factory :shop do
    sequence :hwid do |n|
      "U078040711d1cd488c5b66d1774efaaaa#{n}"
    end  
    name 'testkun'
    description 'testtest'
    url 'https://satori-lab.jp/'
  end
end
