require 'rails_helper'

RSpec.describe Shop, type: :model do
  let(:shop) { build(:shop) }
  describe 'shop model parse_base64 method' do
    context '正しいBase64が送られてきた場合' do
      b64_image = File.read(Rails.root.join('test', 'files', 'sample.txt'))
      it '画像を保存できること' do
        shop.image_contents = b64_image
        expect { shop.save }.to change(Shop, :count).by(1)
        expect(shop.image.attached?).to eq true
      end
    end
  end
end
