require 'rails_helper'

RSpec.describe 'Shops', type: :request do
  describe 'GET #index' do
    let!(:shop1) { create(:shop, name: "hoge1") }
    let!(:shop2) { create(:shop, name: "hoge2") }
    let!(:shop3) { create(:shop, name: "fuga1") }

    context "nameを指定している場合" do
      subject { get api_shops_path, params: { name: "hoge" } }
      it "検索した条件のhwidsが返ってくる" do
        subject
        json = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json["hwids"][0]["hwid"]).to eq shop1.hwid
        expect(json["hwids"][1]["hwid"]).to eq shop2.hwid
        expect(json["hwids"][2]).to eq nil
      end
    end

    context "nameを指定していない場合" do
      subject { get api_shops_path }
      it "３つのhwidsが返ってくる" do
        subject
        json = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json["hwids"][0]["hwid"]).to eq shop1.hwid
        expect(json["hwids"][1]["hwid"]).to eq shop2.hwid
        expect(json["hwids"][2]["hwid"]).to eq shop3.hwid
      end
    end
  end

  describe 'GET #show' do
    let!(:shop) { create(:shop) }
    context '指定したhwidが存在している場合' do
      subject { get api_shop_path(shop.hwid) }
      it '正しいステータスコードが返ってくること' do
        subject
        expect(response.status).to eq 200
      end
      it 'shopの情報が返ってくること' do
        subject
        json = JSON.parse(response.body)
        expect(json["shop"]["name"]).to eq shop.name
        expect(json["shop"]["description"]).to eq shop.description
      end
    end
    context '指定したhwidが存在していない場合' do
      subject { get api_shop_path("komeiji") }
      it 'Not foundが返ってくること' do
        subject
        expect(response.status).to eq 404
        json = JSON.parse(response.body)
        expect(json['error']['messages'][0]).to eq "Not found"
      end
    end
  end
  describe 'POST #create' do
    let(:shop) { build(:shop) }
    context '正しいパラメータの場合' do
      subject { post api_shops_url(format: :json), params: { shop: { hwid: shop.hwid, name: shop.name, description: shop.description, url: shop.url } } }
      it '正しいステータスコードが返ってくること' do
        subject
        expect(response.status).to eq 201
      end
      it 'shopが追加されること' do
        expect { subject }.to change(Shop, :count).by(1)
      end

      it 'imageが追加できること' do
        b64_image = File.read(Rails.root.join('test', 'files', 'sample.txt'))
        post_request = post api_shops_url(format: :json), params: { shop: { hwid: shop.hwid, name: shop.name, description: shop.description, url: shop.url, image_contents: b64_image } }
        expect(response.status).to eq 201
        get_request = get api_shop_url(shop.hwid)
        json = JSON.parse(response.body)
        expect(json['shop']['image']).to match(/\.jpe?g$/)
      end
    end

    context '不正なパラメータの場合' do
      it 'hwidが重複したとき' do
        shop.save
        request = post api_shops_url(format: :json), params: { shop: { hwid: shop.hwid, name: shop.name, description: shop.description } }
        expect { request }.to change(Shop, :count).by(0)
        json = JSON.parse(response.body)
        expect(json['error']['messages'][0]).to eq "Hwid has already been taken"

      end
      it 'hwidがないとき' do
        post api_shops_url(format: :json), params: { shop: { name: shop.name, description: shop.description } }
        expect(response.status).to eq 400
        json = JSON.parse(response.body)
        expect(json['error']['messages'][0]).to eq "Hwid can't be blank"
      end

      it 'descriptionがないとき' do
        post api_shops_url(format: :json), params: { shop: { hwid: shop.hwid, name: shop.name } }
        expect(response.status).to eq 400
        json = JSON.parse(response.body)
        expect(json['error']['messages'][0]).to eq "Description can't be blank"
      end

      it 'nameがないとき' do
        post api_shops_url(format: :json), params: { shop: { hwid: shop.hwid, description: shop.description } }
        expect(response.status).to eq 400
        json = JSON.parse(response.body)
        expect(json['error']['messages'][0]).to eq "Name can't be blank"
      end
    end
  end
end
